<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Document</title>
    <?php
        $this->load->view('recursos/css');
        $this->load->view('recursos/js');
    ?>
</head>
<body class="bg-light">
        <?php
            $this->load->view('menu/menu');
        ?>
        <content class="row m-0 p-0">
        	<div class="col-12 pt-5 pb-5">
        		<?php	
	            	if(isset($_view) && $_view)
					$this->load->view($_view);
				?>
        	</div>
        </content>
        <footer class="footer align-self-end">
            <?php 
            $this->load->view('footer/footer'); 
            ?>
        </footer>
</body>
</html>