<?php echo form_open('usuario_permiso/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="idusuario" class="col-md-4 control-label">Idusuario</label>
		<div class="col-md-8">
			<input type="text" name="idusuario" value="<?php echo $this->input->post('idusuario'); ?>" class="form-control" id="idusuario" />
		</div>
	</div>
	<div class="form-group">
		<label for="idpermiso" class="col-md-4 control-label">Idpermiso</label>
		<div class="col-md-8">
			<input type="text" name="idpermiso" value="<?php echo $this->input->post('idpermiso'); ?>" class="form-control" id="idpermiso" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>