<div class="pull-right">
	<a href="<?php echo site_url('usuario_permiso/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idusuario Permiso</th>
		<th>Idusuario</th>
		<th>Idpermiso</th>
		<th>Actions</th>
    </tr>
	<?php foreach($usuario_permisos as $u){ ?>
    <tr>
		<td><?php echo $u['idusuario_permiso']; ?></td>
		<td><?php echo $u['idusuario']; ?></td>
		<td><?php echo $u['idpermiso']; ?></td>
		<td>
            <a href="<?php echo site_url('usuario_permiso/edit/'.$u['idusuario_permiso']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('usuario_permiso/remove/'.$u['idusuario_permiso']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
