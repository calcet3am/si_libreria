<?php echo form_open('usuario_permiso/edit/'.$usuario_permiso['idusuario_permiso'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="idusuario" class="col-md-4 control-label">Idusuario</label>
		<div class="col-md-8">
			<input type="text" name="idusuario" value="<?php echo ($this->input->post('idusuario') ? $this->input->post('idusuario') : $usuario_permiso['idusuario']); ?>" class="form-control" id="idusuario" />
		</div>
	</div>
	<div class="form-group">
		<label for="idpermiso" class="col-md-4 control-label">Idpermiso</label>
		<div class="col-md-8">
			<input type="text" name="idpermiso" value="<?php echo ($this->input->post('idpermiso') ? $this->input->post('idpermiso') : $usuario_permiso['idpermiso']); ?>" class="form-control" id="idpermiso" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>