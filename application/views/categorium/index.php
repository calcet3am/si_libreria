<div class="pull-right">
	<a href="<?php echo site_url('categoria/add'); ?>" class="btn btn-success">Add</a> 
</div>

<!--<table class="table table-striped table-bordered">
    <tr>
		<th>Idcategoria</th>
		<th>Condicion</th>
		<th>Nombre</th>
		<th>Descripcion</th>
		<th>Actions</th>
    </tr>
	<?php// foreach($categorias as $c){ ?>
    <tr>
		<td><?php// echo $c['idcategoria']; ?></td>
		<td><?php// echo $c['condicion']; ?></td>
		<td><?php// echo $c['nombre']; ?></td>
		<td><?php// echo $c['descripcion']; ?></td>
		<td>
            <a href="<?php// echo site_url('categoria/edit/'.$c['idcategoria']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php//echo site_url('categoria/remove/'.$c['idcategoria']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php// } ?>
</table>
-->
<div class="text-center">
		<h1>LISTA DE CATEGORIAS</h1>
	</div>
	<div class="container">
		<div class="row text-center bg-primary text-light p-2">
			<div class="col-md-2">Nombre</div>
			<div class="col-md-2">Condicion</div>
			<div class="col-md-4">Descripcion</div>
			<div class="col-md-2">Acciones</div>
		</div>
		
		<div class=" text-center">
		<?php foreach($categorias as $c){ ?>
		<div class="row border bg-white">
			<div class="col"><?php echo $c['nombre']; ?></div>
			<div class="col"><?php echo $c['condicion']; ?></div>
			<div class="col"><?php echo $c['descripcion']; ?></div>
			<div class="col text">
				<!-- <span id ="modificar_articulo" class="btn btn-warning btn-sm">Actualizar</span> -->
				<a href="<?=site_url('categoria/remove/'.$c['idcategoria']); ?>" title=""><span id ="eliminar_articulo" class="btn btn-danger btn-sm">Eliminar</span></a>
			</div>
		</div>
		
		<?php } ?>
		</div>
	</div>
	
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
