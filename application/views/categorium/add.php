<?php echo form_open('categoria/add',array("class"=>"form-horizontal")); ?>
<div class="text-center">
	<h1>INGRESAR CATEGORIA</h1>
</div>
<div class="container">

	<div class="row">
		<div class="group-form col-md-8">
			<label for="nombre" class="control-label">Nombre</label>
			<div>
				<input type="text" name="nombre" value="<?php echo $this->input->post('nombre'); ?>" class="form-control" id="nombre" placeholder="Nombre"/>
			</div>
		</div>
		<div class="group-form text-center col-md-4">
				<label for="condicion" class="control-label">Condicion</label>
				<div>
					<input type="checkbox" name="condicion" checked value="1" id="condicion" class="form-control"/>
				</div>
		</div>
	</div>

	<div class="group-form">
		<label for="descripcion" class="control-label">Descripcion</label>
		<div>
			<input type="text" name="descripcion" value="<?php echo $this->input->post('descripcion'); ?>" class="form-control" id="descripcion" placeholder="Descripcion"/>
		</div>
	</div>
	<br>
	<br>
	<br>
	<div class="form-group text-center">
		<div class="col-md-12">
			<button type="submit" class="btn btn-success" id="agregar_categoria" >Guardar</button>
        </div>
	</div>

</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#agregar_categoria').click(function(event) {
			valor = true;
			if (!$('#nombre').val()) {
				alert("No se olvide del nombre");
				$('#nombre').focus();
			valor = false;
			}
			else if (!$('#descripcion').val()) {
				alert("No se olvide de la Descripcion");
				$('#descripcion').focus();
			valor = false;
			}
			return valor;
		});
		});
	</script>