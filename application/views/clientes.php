<?php 
include 'header01.php'; ?>


<h1 class="text-center">REGISTRAR CLIENTE</h1>
<form>
<div class="container">

    <div class="form-group">
      <label>Nombre</label>
      <input type="text" class="form-control" id="nombre_cliente" placeholder="Nombre">
    </div>
    <div class="row">
  <div class="form-group col-md-4">
    <label>Tipo de Persona</label>
    <select name="tipo_persona" id="tipo_persona" class="form-control">
      <option value="0">--Selecccione--</option>
    </select>
  </div>

    <div class="form-group col-md-4">
    <label for="tipo_doc">Tipo de Documento</label>
      <select name="tipo_doc" id="tipo_doc" class="form-control">
        <option value="0">--Seleccione--</option>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label>Documento</label>
      <input type="text" class="form-control" id="documento_cliente" placeholder="Documento">
    </div>
  </div>
  <div class="row">
  <div class="form-group col-md-4">
    <label>Direccion</label>
    <input type="text" class="form-control" id="direccion_cliente" placeholder="Direccion">
  </div>
  <div class="form-group col-md-4">
    <label>Telefono</label>
    <input type="text" class="form-control" id="telefono_cliente" placeholder="Telefono / Celular">
  </div>
  <div class="form-group col-md-4">
    <label>Email</label>
    <input type="text" class="form-control" id="email_cliente" placeholder="Email">
  </div>
  </div>
  <div class="form-group">
      <label>Razon Social</label>
      <input type="text" class="form-control" id="razon_cliente" placeholder="Razon Social">
    </div>
 
  <div class="text-center">
  <br>
  <button id="registrar_articulo" class="btn btn-primary">REGISTRAR</button>
  <button id="cancelar" class="btn btn-danger">CANCELAR</button>
  <br>
  </div> 
</div>
</form>

<?php include 'footer.php'; ?>