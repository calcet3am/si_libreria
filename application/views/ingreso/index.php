<div class="pull-right">
	<a href="<?php echo site_url('ingreso/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idingreso</th>
		<th>Idpersona</th>
		<th>Idusuario</th>
		<th>Tipo Comprobante</th>
		<th>Serie Comprobante</th>
		<th>Num Comprobante</th>
		<th>Fecha Hora</th>
		<th>Impuesto</th>
		<th>Total Compra</th>
		<th>Estado</th>
		<th>Actions</th>
    </tr>
	<?php foreach($ingresos as $i){ ?>
    <tr>
		<td><?php echo $i['idingreso']; ?></td>
		<td><?php echo $i['idpersona']; ?></td>
		<td><?php echo $i['idusuario']; ?></td>
		<td><?php echo $i['tipo_comprobante']; ?></td>
		<td><?php echo $i['serie_comprobante']; ?></td>
		<td><?php echo $i['num_comprobante']; ?></td>
		<td><?php echo $i['fecha_hora']; ?></td>
		<td><?php echo $i['impuesto']; ?></td>
		<td><?php echo $i['total_compra']; ?></td>
		<td><?php echo $i['estado']; ?></td>
		<td>
            <a href="<?php echo site_url('ingreso/edit/'.$i['idingreso']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('ingreso/remove/'.$i['idingreso']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
