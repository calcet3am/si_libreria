<?php echo form_open('ingreso/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="idpersona" class="col-md-4 control-label">Idpersona</label>
		<div class="col-md-8">
			<input type="text" name="idpersona" value="<?php echo $this->input->post('idpersona'); ?>" class="form-control" id="idpersona" />
		</div>
	</div>
	<div class="form-group">
		<label for="idusuario" class="col-md-4 control-label">Idusuario</label>
		<div class="col-md-8">
			<input type="text" name="idusuario" value="<?php echo $this->input->post('idusuario'); ?>" class="form-control" id="idusuario" />
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_comprobante" class="col-md-4 control-label">Tipo Comprobante</label>
		<div class="col-md-8">
			<input type="text" name="tipo_comprobante" value="<?php echo $this->input->post('tipo_comprobante'); ?>" class="form-control" id="tipo_comprobante" />
		</div>
	</div>
	<div class="form-group">
		<label for="serie_comprobante" class="col-md-4 control-label">Serie Comprobante</label>
		<div class="col-md-8">
			<input type="text" name="serie_comprobante" value="<?php echo $this->input->post('serie_comprobante'); ?>" class="form-control" id="serie_comprobante" />
		</div>
	</div>
	<div class="form-group">
		<label for="num_comprobante" class="col-md-4 control-label">Num Comprobante</label>
		<div class="col-md-8">
			<input type="text" name="num_comprobante" value="<?php echo $this->input->post('num_comprobante'); ?>" class="form-control" id="num_comprobante" />
		</div>
	</div>
	<div class="form-group">
		<label for="fecha_hora" class="col-md-4 control-label">Fecha Hora</label>
		<div class="col-md-8">
			<input type="text" name="fecha_hora" value="<?php echo $this->input->post('fecha_hora'); ?>" class="form-control" id="fecha_hora" />
		</div>
	</div>
	<div class="form-group">
		<label for="impuesto" class="col-md-4 control-label">Impuesto</label>
		<div class="col-md-8">
			<input type="text" name="impuesto" value="<?php echo $this->input->post('impuesto'); ?>" class="form-control" id="impuesto" />
		</div>
	</div>
	<div class="form-group">
		<label for="total_compra" class="col-md-4 control-label">Total Compra</label>
		<div class="col-md-8">
			<input type="text" name="total_compra" value="<?php echo $this->input->post('total_compra'); ?>" class="form-control" id="total_compra" />
		</div>
	</div>
	<div class="form-group">
		<label for="estado" class="col-md-4 control-label">Estado</label>
		<div class="col-md-8">
			<input type="text" name="estado" value="<?php echo $this->input->post('estado'); ?>" class="form-control" id="estado" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>