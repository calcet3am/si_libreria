<?php 
include 'header01.php'; ?>

<h1 class="text-center">REGISTRAR ARTICULO</h1>
<form>
<div class="container">

    <div class="form-group">
      <label>Nombre</label>
      <input type="text" class="form-control" id="nombre_articulo" placeholder="Nombre">
    </div>
    <div class="form-group">
      <label>Descripcion</label>
      <input type="text" class="form-control" id="desc_articulo" placeholder="Descripcion">
    </div>
  <div class="row">
  <div class="form-group col-md-4">
    <label>Codigo</label>
    <input type="text" class="form-control" id="codigo_articulo" placeholder="Codigo">
  </div>
  <div class="form-group col-md-4">
    <label>Categoria</label>
    <input type="text" class="form-control" id="categoria_articulo" placeholder="Categoria">
  </div>
  <div class="form-group col-md-4">
    <label>Precio</label>
    <input type="text" class="form-control" id="precio_articulo" placeholder="Precio">
  </div>
  </div>
  
  <div class="text-center">
  <br>
  <button id="registrar_articulo" class="btn btn-primary">REGISTRAR</button>
  <button id="cancelar" class="btn btn-danger">CANCELAR</button>
  <br>
  </div> 
</div>
</form>


<?php include 'footer.php'; ?>