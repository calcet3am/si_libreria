
<?php echo form_open('ventas/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="cantidad" class="col-md-4 control-label">idventa</label>
		<div class="col-md-8">
			<input type="text" name="idventa" value="<?php echo $this->input->post('idventa'); ?>" class="form-control" id="cantidad" />
		</div>
	</div>
	<div class="form-group">
		<label for="descuento" class="col-md-4 control-label">Descuento</label>
		<div class="col-md-8">
			<input type="text" name="descuento" value="<?php echo $this->input->post('descuento'); ?>" class="form-control" id="descuento" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>