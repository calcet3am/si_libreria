<?php echo form_open('ventas/edit/'.$venta['idventa'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="cantidad" class="col-md-4 control-label">Cantidad</label>
		<div class="col-md-8">
			<input type="text" name="cantidad" value="<?php echo ($this->input->post('cantidad') ? $this->input->post('cantidad') : $venta['cantidad']); ?>" class="form-control" id="cantidad" />
		</div>
	</div>
	<div class="form-group">
		<label for="descuento" class="col-md-4 control-label">Descuento</label>
		<div class="col-md-8">
			<input type="text" name="descuento" value="<?php echo ($this->input->post('descuento') ? $this->input->post('descuento') : $venta['descuento']); ?>" class="form-control" id="descuento" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>