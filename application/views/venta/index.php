<div class="pull-right">
	<a href="<?php echo site_url('ventas/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idventa</th>
		<th>Cantidad</th>
		<th>Descuento</th>
		<th>Actions</th>
    </tr>
	<?php foreach($ventas as $v){ ?>
    <tr>
		<td><?php echo $v['idventa']; ?></td>
		<td><?php echo $v['cantidad']; ?></td>
		<td><?php echo $v['descuento']; ?></td>
		<td>
            <a href="<?php echo site_url('ventas/edit/'.$v['idventa']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('ventas/remove/'.$v['idventa']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
