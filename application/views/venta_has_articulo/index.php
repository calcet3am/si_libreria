<div class="pull-right">
	<a href="<?php echo site_url('venta_has_articulo/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idventa</th>
		<th>Idarticulo</th>
		<th>Actions</th>
    </tr>
	<?php foreach($venta_has_articulos as $v){ ?>
    <tr>
		<td><?php echo $v['idventa']; ?></td>
		<td><?php echo $v['idarticulo']; ?></td>
		<td>
            <a href="<?php echo site_url('venta_has_articulo/edit/'.$v['idventa']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('venta_has_articulo/remove/'.$v['idventa']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
