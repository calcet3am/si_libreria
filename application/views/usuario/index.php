<div class="pull-right">
	<a href="<?php echo site_url('usuario/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idusuario</th>
		<th>Condicion</th>
		<th>Nombre</th>
		<th>Tipo Documento</th>
		<th>Num Documento</th>
		<th>Direccion</th>
		<th>Telefono</th>
		<th>Email</th>
		<th>Cargo</th>
		<th>Login</th>
		<th>Clave</th>
		<th>Actions</th>
    </tr>
	<?php foreach($usuarios as $u){ ?>
    <tr>
		<td><?php echo $u['idusuario']; ?></td>
		<td><?php echo $u['condicion']; ?></td>
		<td><?php echo $u['nombre']; ?></td>
		<td><?php echo $u['tipo_documento']; ?></td>
		<td><?php echo $u['num_documento']; ?></td>
		<td><?php echo $u['direccion']; ?></td>
		<td><?php echo $u['telefono']; ?></td>
		<td><?php echo $u['email']; ?></td>
		<td><?php echo $u['cargo']; ?></td>
		<td><?php echo $u['login']; ?></td>
		<td><?php echo $u['clave']; ?></td>
		<td>
            <a href="<?php echo site_url('usuario/edit/'.$u['idusuario']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('usuario/remove/'.$u['idusuario']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
