<?php echo form_open('usuario/edit/'.$usuario['idusuario'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="condicion" class="col-md-4 control-label">Condicion</label>
		<div class="col-md-8">
			<input type="checkbox" name="condicion" value="1" <?php echo ($usuario['condicion']==1 ? 'checked="checked"' : ''); ?> id='condicion' />
		</div>
	</div>
	<div class="form-group">
		<label for="nombre" class="col-md-4 control-label">Nombre</label>
		<div class="col-md-8">
			<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $usuario['nombre']); ?>" class="form-control" id="nombre" />
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_documento" class="col-md-4 control-label">Tipo Documento</label>
		<div class="col-md-8">
			<input type="text" name="tipo_documento" value="<?php echo ($this->input->post('tipo_documento') ? $this->input->post('tipo_documento') : $usuario['tipo_documento']); ?>" class="form-control" id="tipo_documento" />
		</div>
	</div>
	<div class="form-group">
		<label for="num_documento" class="col-md-4 control-label">Num Documento</label>
		<div class="col-md-8">
			<input type="text" name="num_documento" value="<?php echo ($this->input->post('num_documento') ? $this->input->post('num_documento') : $usuario['num_documento']); ?>" class="form-control" id="num_documento" />
		</div>
	</div>
	<div class="form-group">
		<label for="direccion" class="col-md-4 control-label">Direccion</label>
		<div class="col-md-8">
			<input type="text" name="direccion" value="<?php echo ($this->input->post('direccion') ? $this->input->post('direccion') : $usuario['direccion']); ?>" class="form-control" id="direccion" />
		</div>
	</div>
	<div class="form-group">
		<label for="telefono" class="col-md-4 control-label">Telefono</label>
		<div class="col-md-8">
			<input type="text" name="telefono" value="<?php echo ($this->input->post('telefono') ? $this->input->post('telefono') : $usuario['telefono']); ?>" class="form-control" id="telefono" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $usuario['email']); ?>" class="form-control" id="email" />
		</div>
	</div>
	<div class="form-group">
		<label for="cargo" class="col-md-4 control-label">Cargo</label>
		<div class="col-md-8">
			<input type="text" name="cargo" value="<?php echo ($this->input->post('cargo') ? $this->input->post('cargo') : $usuario['cargo']); ?>" class="form-control" id="cargo" />
		</div>
	</div>
	<div class="form-group">
		<label for="login" class="col-md-4 control-label">Login</label>
		<div class="col-md-8">
			<input type="text" name="login" value="<?php echo ($this->input->post('login') ? $this->input->post('login') : $usuario['login']); ?>" class="form-control" id="login" />
		</div>
	</div>
	<div class="form-group">
		<label for="clave" class="col-md-4 control-label">Clave</label>
		<div class="col-md-8">
			<input type="text" name="clave" value="<?php echo ($this->input->post('clave') ? $this->input->post('clave') : $usuario['clave']); ?>" class="form-control" id="clave" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>