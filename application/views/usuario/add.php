<?php echo json_encode($permisos); ?>
<?php echo form_open('usuario/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="condicion" class="col-md-4 control-label">Condicion</label>
		<div class="col-md-8">
			<input type="checkbox" name="condicion" checked value="1" id="condicion" />
		</div>
	</div>
	<div class="form-group">
    <label>permisos de usuario</label>
    <select name="idpermiso" class="form-control">
				<option value="">permisos de usuario</option>
				<?php 
				foreach($permisos as $permiso)
				{
					$selected = ($permiso['idpermiso'] == $this->input->post('idpermiso')) ? ' selected="selected"' : "";

					echo '<option value="'.$permiso['idpermiso'].'" '.$selected.'>'.$permiso['nombre'].'</option>';
				} 
				?>
			</select>
  </div>
	<div class="form-group">
		<label for="nombre" class="col-md-4 control-label">Nombre</label>
		<div class="col-md-8">
			<input type="text" name="nombre" value="<?php echo $this->input->post('nombre'); ?>" class="form-control" id="nombre" />
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_documento" class="col-md-4 control-label">Tipo Documento</label>
		<div class="col-md-8">
			<input type="text" name="tipo_documento" value="<?php echo $this->input->post('tipo_documento'); ?>" class="form-control" id="tipo_documento" />
		</div>
	</div>
	<div class="form-group">
		<label for="num_documento" class="col-md-4 control-label">Num Documento</label>
		<div class="col-md-8">
			<input type="text" name="num_documento" value="<?php echo $this->input->post('num_documento'); ?>" class="form-control" id="num_documento" />
		</div>
	</div>
	<div class="form-group">
		<label for="direccion" class="col-md-4 control-label">Direccion</label>
		<div class="col-md-8">
			<input type="text" name="direccion" value="<?php echo $this->input->post('direccion'); ?>" class="form-control" id="direccion" />
		</div>
	</div>
	<div class="form-group">
		<label for="telefono" class="col-md-4 control-label">Telefono</label>
		<div class="col-md-8">
			<input type="text" name="telefono" value="<?php echo $this->input->post('telefono'); ?>" class="form-control" id="telefono" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
		</div>
	</div>
	<div class="form-group">
		<label for="cargo" class="col-md-4 control-label">Cargo</label>
		<div class="col-md-8">
			<input type="text" name="cargo" value="<?php echo $this->input->post('cargo'); ?>" class="form-control" id="cargo" />
		</div>
	</div>
	<div class="form-group">
		<label for="login" class="col-md-4 control-label">Login</label>
		<div class="col-md-8">
			<input type="text" name="login" value="<?php echo $this->input->post('login'); ?>" class="form-control" id="login" />
		</div>
	</div>
	<div class="form-group">
		<label for="clave" class="col-md-4 control-label">Clave</label>
		<div class="col-md-8">
			<input type="text" name="clave" value="<?php echo $this->input->post('clave'); ?>" class="form-control" id="clave" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>