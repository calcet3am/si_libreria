<div class="pull-right">
	<a href="<?php echo site_url('detalle_venta/add'); ?>" class="btn btn-success">Add</a> 
</div>
<?php echo "<script> var detalle_venta_articulos=JSON.parse(`".json_encode($detalle_venta_articulos)."`); 
			var detalle_ventas=JSON.parse(`".json_encode($detalle_ventas)."`); 
			console.log('detalle_ventas',detalle_ventas)</script>"; 
?>

<!--<table class="table table-striped table-bordered">
    <tr>
		<th>Iddetalle Venta</th>
		<th>Idventa</th>
		<th>Idpersona</th>
		<th>Idusuario</th>
		<th>Tipo Comprobante</th>
		<th>Serie Comprobante</th>
		<th>Num Comprobante</th>
		<th>Fecha Hora</th>
		<th>Impuesto</th>
		<th>Total Venta</th>
		<th>Estado</th>
		<th>Actions</th>
    </tr>
	<?php// foreach($detalle_ventas as $d){ ?>
    <tr>
		<td><?php// echo $d['iddetalle_venta']; ?></td>
		<td><?php// echo $d['idventa']; ?></td>
		<td><?php// echo $d['idpersona']; ?></td>
		<td><?php// echo $d['idusuario']; ?></td>
		<td><?php// echo $d['tipo_comprobante']; ?></td>
		<td><?php// echo $d['serie_comprobante']; ?></td>
		<td><?php// echo $d['num_comprobante']; ?></td>
		<td><?php// echo $d['fecha_hora']; ?></td>
		<td><?php// echo $d['impuesto']; ?></td>
		<td><?php// echo $d['total_venta']; ?></td>
		<td><?php// echo $d['estado']; ?></td>
		<td>
            <a href="<?php// echo site_url('detalle_venta/edit/'.$d['iddetalle_venta']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php// echo site_url('detalle_venta/remove/'.$d['iddetalle_venta']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php// } ?>
</table>
-->

<div class=" text-center">
		<h1>REGISTRO DE VENTAS</h1>
	</div>
	<div class="container">
		<div class="row text-center bg-primary text-light p-2">
			<div class="col-md-1">Numero pedido</div>
			<div class="col-md-3">Cliente</div>
			<div class="col-md-3">Vendedor</div>
			<div class="col-md-2">Fecha</div>
			<div class="col-md-1">Precio Total</div>
			<div class="col-md-2">Acciones</div>
		</div>
		
		
		<?php foreach($detalle_ventas as $d){ ?>
		<div class="row bg-white border text-center">
			<div class="col-md-1"><?php echo $d['iddetalle_venta']; ?></div>
			<div class="col-md-3"><?php echo $d['idpersona']; ?></div>
			<div class="col-md-3"><?php echo $d['idusuario']; ?></div>
			<div class="col-md-2"><?php echo $d['fecha_hora']; ?></div>
			<div class="col-md-1"><?php echo $d['total_venta']; ?></div>
			<div class="col-md-2">
					<!-- <button id ="modificar_articulo" class="btn btn-warning">Actualizar</button> -->
					<span class="btn btn-success btn-sm btn_detalle_venta" data-idventa="<?=$d['iddetalle_venta'];?>">Detalles</span>
					<a href="<?=site_url('detalle_venta/remove/'.$d['iddetalle_venta']); ?>" class="btn btn-danger btn-sm">Delete</a>
				</div>
		</div>
			<?php } ?>
			
	</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="pull-right">
    
</div>
<script type="text/javascript">
	$(document).on('click', '.btn_detalle_venta', function(event) {
		event.preventDefault();
		var text_pedidos=`<div class="row text-center bg-primary text-light">
							<div class="col">Cliente</div>
							<div class="col">Fecha y hora</div>
						</div>
						<div class="row text-center bg-white border mb-3">
							<div class="col">${detalle_ventas[$(this).data('idventa')].idpersona}</div>
							<div class="col">${detalle_ventas[$(this).data('idventa')].fecha_hora}</div>
						</div>
						<div class="row bg-primary text-light div-txt-search-destiny p-2" >
				           <div class="col">Nombre Articulo</div>
				            <div class="col">Precio/unidad</div>
				            <div class="col">Cantidad</div>
				       		<div class="col">Precio</div>
				          </div>
				        `;
		// console.log($(this).data('idventa'));
		detalle_venta_articulos[$(this).data('idventa')].forEach(function(value,index){
			text_pedidos = text_pedidos + `
			          <div class="row border bg-white" >
			           <div class="col">${value.articulo.nombre}
			            </div>
			            <div class="col">${value.articulo.precio}</div>
			            <div class="col">${value.cantidad}</div>
			            <div class="col">${value.cantidad*value.articulo.precio}</div>
			          </div>
			        `;
		});
		text_pedidos = text_pedidos+`
						<div class="row text-center bg-secondary text-light p-2">
							<div class="col">Impuestos</div>
							<div class="col">${detalle_ventas[$(this).data('idventa')].impuesto}</div>
						</div>
						<div class="row text-center bg-danger text-light p-2">
							<div class="col">TOTAL</div>
							<div class="col font-weight-bold">${detalle_ventas[$(this).data('idventa')].total_venta}</div>
						</div>
						`;

		$('#exampleModal').modal('show')

		$('#exampleModal .modal-body').html(text_pedidos);
		console.log(text_pedidos);


		
		/* Act on the event */
	});
</script>