<div class="pull-right">
	<a href="<?php echo site_url('detalle_ingreso/add'); ?>" class="btn btn-success">Add</a> 
</div>

<!--<table class="table table-striped table-bordered">
    <tr>
		<th>Iddetalle Ingreso</th>
		<th>Idingreso</th>
		<th>Idarticulo</th>
		<th>Cantidad</th>
		<th>Precio Compra</th>
		<th>Precio Venta</th>
		<th>Actions</th>
    </tr>
	<?php// foreach($detalle_ingresos as $d){ ?>
    <tr>
		<td><?php// echo $d['iddetalle_ingreso']; ?></td>
		<td><?php// echo $d['idingreso']; ?></td>
		<td><?php// echo $d['idarticulo']; ?></td>
		<td><?php// echo $d['cantidad']; ?></td>
		<td><?php// echo $d['precio_compra']; ?></td>
		<td><?php// echo $d['precio_venta']; ?></td>
		<td>
            <a href="<?php// echo site_url('detalle_ingreso/edit/'.$d['iddetalle_ingreso']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php// echo site_url('detalle_ingreso/remove/'.$d['iddetalle_ingreso']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php// } ?>
</table>
-->
<div class=" text-center">
		<h1>LISTA DE ARTICULOS</h1>
	</div>
	<div class="container">
		<div class="row text-center bg-primary text-light p-2">
			<div class="col-md-2">Numero</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">Descripcion</div>
			<div class="col-md-1">Stock</div>
			<div class="col-md-1">Precio</div>
			<div class="col-md-2">Acciones</div>
		</div>
		
		<div class="row">
		<?php foreach($detalle_ingreso as $d){ ?>
		<div class="col-md-2"><?php echo $d['nombre']; ?></div>
		<div class="col-md-2"><?php echo $d['idcategoria']; ?></div>
		<div class="col-md-4"><?php echo $d['descripcion']; ?></div>
		<div class="col-md-1"><?php echo $d['stock']; ?></div>
		<div class="col-md-1"><?php echo $d['precio']; ?></div>
		<div class="col-md-2">
				<button id ="modificar_articulo" class="btn btn-warning">Actualizar</button>
				<button id ="eliminar_articulo" class="btn btn-danger">Eliminar</button>
			</div>
			<?php } ?>
			</div>
	</div>

<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
