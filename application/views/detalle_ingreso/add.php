<?php echo form_open('detalle_ingreso/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="idingreso" class="col-md-4 control-label">Idingreso</label>
		<div class="col-md-8">
			<input type="text" name="idingreso" value="<?php echo $this->input->post('idingreso'); ?>" class="form-control" id="idingreso" />
		</div>
	</div>
	<div class="form-group">
		<label for="idarticulo" class="col-md-4 control-label">Idarticulo</label>
		<div class="col-md-8">
			<input type="text" name="idarticulo" value="<?php echo $this->input->post('idarticulo'); ?>" class="form-control" id="idarticulo" />
		</div>
	</div>
	<div class="form-group">
		<label for="cantidad" class="col-md-4 control-label">Cantidad</label>
		<div class="col-md-8">
			<input type="text" name="cantidad" value="<?php echo $this->input->post('cantidad'); ?>" class="form-control" id="cantidad" />
		</div>
	</div>
	<div class="form-group">
		<label for="precio_compra" class="col-md-4 control-label">Precio Compra</label>
		<div class="col-md-8">
			<input type="text" name="precio_compra" value="<?php echo $this->input->post('precio_compra'); ?>" class="form-control" id="precio_compra" />
		</div>
	</div>
	<div class="form-group">
		<label for="precio_venta" class="col-md-4 control-label">Precio Venta</label>
		<div class="col-md-8">
			<input type="text" name="precio_venta" value="<?php echo $this->input->post('precio_venta'); ?>" class="form-control" id="precio_venta" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>