<nav class=" navbar sticky-top navbar-expand-lg navbar-dark bg-primary text-light navbar-main">
        <div class="container">
            <a class="navbar-brand " href="#"><span >App</span><span class="font-weight-bold">Libreria</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?=base_url()?>">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>articulo">Articulo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>categoria">categoria</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>detalle_venta">Ventas</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link disabled" href="#mdo">Reportes</a>
                    </li> -->
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>persona">Personas</a>
                    </li> -->
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>detalle_ingreso">compras</a>
                    </li> -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Configuración
              </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?=base_url()?>permiso">Permisos</a>
                            <a class="dropdown-item" href="<?=base_url()?>usuario">usuarios</a>
                            <!-- <div role="separator" class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#three">three</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a> -->
                        </div>
                    </li>
                </ul>

            </div>
        </div>

    </nav>