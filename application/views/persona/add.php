<?php echo form_open('persona/add',array("class"=>"form-horizontal")); ?>

	<!--<div class="form-group">
		<label for="tipo_persona" class="col-md-4 control-label">Tipo Persona</label>
		<div class="col-md-8">
			<input type="text" name="tipo_persona" value="<?php// echo $this->input->post('tipo_persona'); ?>" class="form-control" id="tipo_persona" />
		</div>
	</div>
	<div class="form-group">
		<label for="nombre" class="col-md-4 control-label">Nombre</label>
		<div class="col-md-8">
			<input type="text" name="nombre" value="<?php// echo $this->input->post('nombre'); ?>" class="form-control" id="nombre" />
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_documento" class="col-md-4 control-label">Tipo Documento</label>
		<div class="col-md-8">
			<input type="text" name="tipo_documento" value="<?php// echo $this->input->post('tipo_documento'); ?>" class="form-control" id="tipo_documento" />
		</div>
	</div>
	<div class="form-group">
		<label for="num_documento" class="col-md-4 control-label">Num Documento</label>
		<div class="col-md-8">
			<input type="text" name="num_documento" value="<?php// echo $this->input->post('num_documento'); ?>" class="form-control" id="num_documento" />
		</div>
	</div>
	<div class="form-group">
		<label for="direccion" class="col-md-4 control-label">Direccion</label>
		<div class="col-md-8">
			<input type="text" name="direccion" value="<?php// echo $this->input->post('direccion'); ?>" class="form-control" id="direccion" />
		</div>
	</div>
	<div class="form-group">
		<label for="telefono" class="col-md-4 control-label">Telefono</label>
		<div class="col-md-8">
			<input type="text" name="telefono" value="<?php// echo $this->input->post('telefono'); ?>" class="form-control" id="telefono" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php//echo $this->input->post('email'); ?>" class="form-control" id="email" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	-->

<h1 class="text-center">REGISTRAR CLIENTE / PROVEEDOR</h1>
<form>
<div class="container">

    <div class="form-group">
      <label>Nombre</label>
      <input type="text" class="form-control" id="nombre_cliente" placeholder="Nombre">
    </div>
    <div class="row">
  <div class="form-group col-md-4">
    <label>Tipo de Persona</label>
    <select name="tipo_persona" id="tipo_persona" class="form-control">
      <option value="0">--Selecccione--</option>
    </select>
  </div>

    <div class="form-group col-md-4">
    <label for="tipo_doc">Tipo de Documento</label>
      <select name="tipo_doc" id="tipo_doc" class="form-control">
        <option value="0">--Seleccione--</option>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label>Documento</label>
      <input type="text" class="form-control" id="documento_cliente" placeholder="Documento">
    </div>
  </div>
  <div class="row">
  <div class="form-group col-md-4">
    <label>Direccion</label>
    <input type="text" class="form-control" id="direccion_cliente" placeholder="Direccion">
  </div>
  <div class="form-group col-md-4">
    <label>Telefono</label>
    <input type="text" class="form-control" id="telefono_cliente" placeholder="Telefono / Celular">
  </div>
  <div class="form-group col-md-4">
    <label>Email</label>
    <input type="text" class="form-control" id="email_cliente" placeholder="Email">
  </div>
  </div>
  <div class="form-group">
      <label>Razon Social</label>
      <input type="text" class="form-control" id="razon_cliente" placeholder="Razon Social">
    </div>
 
  <div class="text-center">
  <br>
  <button id="agregar_articulo" class="btn btn-primary">REGISTRAR</button>
  <button id="cancelar" class="btn btn-danger">CANCELAR</button>
  <br>
  </div> 
</div>
</form>
<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#agregar_articulo').click(function(event) {
			valor = true;
			if (!$('#nombre_cliente').val()) {
				alert("No se olvide del nombre del Cliente");
				$('#nombre_cliente').focus();
			valor = false;
			}
			else if ($('#tipo_persona').val()=="0") {
				alert("No se olvide del tipo de Persona");
				$('#tipo_persona').focus();
			valor = false;
			}
			else if ($('#tipo_doc').val()=="0") {
				alert("No se olvide del Tipo de Documento");
				$('#tipo_doc').focus();
			valor = false;
			}
			else if (!$('#documento_cliente').val()) {
				alert("No se olvide del documento del Cliente");
				$('#documento_cliente').focus();
			valor = false;
			}
			else if (!$('#direccion_cliente').val()) {
				alert("No se olvide de la direccion del Cliente");
				$('#direccion_cliente').focus();
			valor = false;
			}
			else if (!$('#telefono_cliente').val()) {
				alert("No se olvide del telefono del Cliente");
				$('#telefono_cliente').focus();
			valor = false;
			}
			else if (!$('#email_cliente').val()) {
				alert("No se olvide del correo del Cliente");
				$('#email_cliente').focus();
			valor = false;
			}
			else if (!$('#razon_cliente').val()) {
				alert("No se olvide de la Razon Social del Cliente");
				$('#razon_cliente').focus();
			valor = false;
			}
			return valor;
		});
		});
	</script>