<div class="pull-right">
	<a href="<?php echo site_url('persona/add'); ?>" class="btn btn-success">Add</a> 
</div>

<!--<table class="table table-striped table-bordered">
    <tr>
		<th>Idpersona</th>
		<th>Tipo Persona</th>
		<th>Nombre</th>
		<th>Tipo Documento</th>
		<th>Num Documento</th>
		<th>Direccion</th>
		<th>Telefono</th>
		<th>Email</th>
		<th>Actions</th>
    </tr>
	<?php// foreach($personas as $p){ ?>
    <tr>
		<td><?php// echo $p['idpersona']; ?></td>
		<td><?php// echo $p['tipo_persona']; ?></td>
		<td><?php// echo $p['nombre']; ?></td>
		<td><?php// echo $p['tipo_documento']; ?></td>
		<td><?php// echo $p['num_documento']; ?></td>
		<td><?php// echo $p['direccion']; ?></td>
		<td><?php// echo $p['telefono']; ?></td>
		<td><?php// echo $p['email']; ?></td>
		<td>
            <a href="<?php// echo site_url('persona/edit/'.$p['idpersona']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php// echo site_url('persona/remove/'.$p['idpersona']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php// } ?>
</table>
-->

<div class=" text-center">
		<h1>CLIENTES Y PROVEEDORES</h1>
	</div>
	<div class="container">
		<div class="row text-center bg-primary text-light p-2">
			<div class="col-md-2">Nombre</div>
			<div class="col-md-2">Tipo Documento</div>
			<div class="col-md-4">Documento</div>
			<div class="col-md-1">Direccion</div>
			<div class="col-md-1">Telefono</div>
			<div class="col-md-1">Email</div>
			<div class="col-md-2">Acciones</div>
		</div>
		
		<div class="row">
		<?php foreach($personas as $p){ ?>
		<div class="col-md-2"><?php echo $p['nombre']; ?></div>
		<div class="col-md-2"><?php echo $p['tipo_persona']; ?></div>
		<div class="col-md-4"><?php echo $p['num_documento']; ?></div>
		<div class="col-md-1"><?php echo $p['direccion']; ?></div>
		<div class="col-md-1"><?php echo $p['telefono']; ?></div>
		<div class="col-md-1"><?php echo $p['email']; ?></div>
		<div class="col-md-2">
				<button id ="modificar_persona" class="btn btn-warning">Actualizar</button>
				<button id ="eliminar_persona" class="btn btn-danger">Eliminar</button>
			</div>
			<?php } ?>
			</div>
	</div>

<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
