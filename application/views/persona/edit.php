<?php echo form_open('persona/edit/'.$persona['idpersona'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="tipo_persona" class="col-md-4 control-label">Tipo Persona</label>
		<div class="col-md-8">
			<input type="text" name="tipo_persona" value="<?php echo ($this->input->post('tipo_persona') ? $this->input->post('tipo_persona') : $persona['tipo_persona']); ?>" class="form-control" id="tipo_persona" />
		</div>
	</div>
	<div class="form-group">
		<label for="nombre" class="col-md-4 control-label">Nombre</label>
		<div class="col-md-8">
			<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $persona['nombre']); ?>" class="form-control" id="nombre" />
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_documento" class="col-md-4 control-label">Tipo Documento</label>
		<div class="col-md-8">
			<input type="text" name="tipo_documento" value="<?php echo ($this->input->post('tipo_documento') ? $this->input->post('tipo_documento') : $persona['tipo_documento']); ?>" class="form-control" id="tipo_documento" />
		</div>
	</div>
	<div class="form-group">
		<label for="num_documento" class="col-md-4 control-label">Num Documento</label>
		<div class="col-md-8">
			<input type="text" name="num_documento" value="<?php echo ($this->input->post('num_documento') ? $this->input->post('num_documento') : $persona['num_documento']); ?>" class="form-control" id="num_documento" />
		</div>
	</div>
	<div class="form-group">
		<label for="direccion" class="col-md-4 control-label">Direccion</label>
		<div class="col-md-8">
			<input type="text" name="direccion" value="<?php echo ($this->input->post('direccion') ? $this->input->post('direccion') : $persona['direccion']); ?>" class="form-control" id="direccion" />
		</div>
	</div>
	<div class="form-group">
		<label for="telefono" class="col-md-4 control-label">Telefono</label>
		<div class="col-md-8">
			<input type="text" name="telefono" value="<?php echo ($this->input->post('telefono') ? $this->input->post('telefono') : $persona['telefono']); ?>" class="form-control" id="telefono" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $persona['email']); ?>" class="form-control" id="email" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>