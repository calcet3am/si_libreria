<div class="pull-right">
	<a href="<?php echo site_url('permiso/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idpermiso</th>
		<th>Nombre</th>
		<th>Actions</th>
    </tr>
	<?php foreach($permisos as $p){ ?>
    <tr>
		<td><?php echo $p['idpermiso']; ?></td>
		<td><?php echo $p['nombre']; ?></td>
		<td>
            <a href="<?php echo site_url('permiso/edit/'.$p['idpermiso']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('permiso/remove/'.$p['idpermiso']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
