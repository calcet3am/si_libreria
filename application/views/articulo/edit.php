<?php echo form_open('articulo/edit/'.$articulo['idarticulo'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="condicion" class="col-md-4 control-label">Condicion</label>
		<div class="col-md-8">
			<input type="checkbox" name="condicion" value="1" <?php echo ($articulo['condicion']==1 ? 'checked="checked"' : ''); ?> id='condicion' />
		</div>
	</div>
	<div class="form-group">
		<label for="idcategoria" class="col-md-4 control-label">Categoria</label>
		<div class="col-md-8">
			<select name="idcategoria" class="form-control">
				<option value="">select categoria</option>
				<?php 
				foreach($all_categorias as $categoria)
				{
					$selected = ($categoria['idcategoria'] == $articulo['idcategoria']) ? ' selected="selected"' : "";

					echo '<option value="'.$categoria['idcategoria'].'" '.$selected.'>'.$categoria['nombre'].'</option>';
				} 
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="codigo" class="col-md-4 control-label">Codigo</label>
		<div class="col-md-8">
			<input type="text" name="codigo" value="<?php echo ($this->input->post('codigo') ? $this->input->post('codigo') : $articulo['codigo']); ?>" class="form-control" id="codigo" />
		</div>
	</div>
	<div class="form-group">
		<label for="nombre" class="col-md-4 control-label">Nombre</label>
		<div class="col-md-8">
			<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $articulo['nombre']); ?>" class="form-control" id="nombre" />
		</div>
	</div>
	<div class="form-group">
		<label for="stock" class="col-md-4 control-label">Stock</label>
		<div class="col-md-8">
			<input type="text" name="stock" value="<?php echo ($this->input->post('stock') ? $this->input->post('stock') : $articulo['stock']); ?>" class="form-control" id="stock" />
		</div>
	</div>
	<div class="form-group">
		<label for="descripcion" class="col-md-4 control-label">Descripcion</label>
		<div class="col-md-8">
			<input type="text" name="descripcion" value="<?php echo ($this->input->post('descripcion') ? $this->input->post('descripcion') : $articulo['descripcion']); ?>" class="form-control" id="descripcion" />
		</div>
	</div>
	<div class="form-group">
		<label for="imagen" class="col-md-4 control-label">Imagen</label>
		<div class="col-md-8">
			<input type="text" name="imagen" value="<?php echo ($this->input->post('imagen') ? $this->input->post('imagen') : $articulo['imagen']); ?>" class="form-control" id="imagen" />
		</div>
	</div>
	<div class="form-group">
		<label for="precio" class="col-md-4 control-label">Precio</label>
		<div class="col-md-8">
			<input type="text" name="precio" value="<?php echo ($this->input->post('precio') ? $this->input->post('precio') : $articulo['precio']); ?>" class="form-control" id="precio" />
		</div>
	</div>
	<div class="form-group">
		<label for="articulocol" class="col-md-4 control-label">Articulocol</label>
		<div class="col-md-8">
			<input type="text" name="articulocol" value="<?php echo ($this->input->post('articulocol') ? $this->input->post('articulocol') : $articulo['articulocol']); ?>" class="form-control" id="articulocol" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>