<?php echo form_open('articulo/add',array("class"=>"form-horizontal")); ?>
	<!--
	<div class="form-group">
		<label for="condicion" class="col-md-4 control-label">Condicion</label>
		<div class="col-md-8">
			<input type="checkbox" name="condicion" value="1" id="condicion" />
		</div>
	</div>
	<div class="form-group">
		<label for="idcategoria" class="col-md-4 control-label">Categoria</label>
		<div class="col-md-8">
			<select name="idcategoria" class="form-control">
				<option value="">select categoria</option>
				<?php 
	//			foreach($all_categorias as $categoria)
				{
	//				$selected = ($categoria['idcategoria'] == $this->input->post('idcategoria')) ? ' selected="selected"' : "";

	//				echo '<option value="'.$categoria['idcategoria'].'" '.$selected.'>'.$categoria['nombre'].'</option>';
				} 
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="codigo" class="col-md-4 control-label">Codigo</label>
		<div class="col-md-8">
			<input type="text" name="codigo" value="<?php //echo $this->input->post('codigo'); ?>" class="form-control" id="codigo" />
		</div>
	</div>
	<div class="form-group">
		<label for="nombre" class="col-md-4 control-label">Nombre</label>
		<div class="col-md-8">
			<input type="text" name="nombre" value="<?php //echo $this->input->post('nombre'); ?>" class="form-control" id="nombre" />
		</div>
	</div>
	<div class="form-group">
		<label for="stock" class="col-md-4 control-label">Stock</label>
		<div class="col-md-8">
			<input type="text" name="stock" value="<?php //cho $this->input->post('stock'); ?>" class="form-control" id="stock" />
		</div>
	</div>
	<div class="form-group">
		<label for="descripcion" class="col-md-4 control-label">Descripcion</label>
		<div class="col-md-8">
			<input type="text" name="descripcion" value="<?php //echo $this->input->post('descripcion'); ?>" class="form-control" id="descripcion" />
		</div>
	</div>
	<div class="form-group">
		<label for="imagen" class="col-md-4 control-label">Imagen</label>
		<div class="col-md-8">
			<input type="text" name="imagen" value="<?php //echo $this->input->post('imagen'); ?>" class="form-control" id="imagen" />
		</div>
	</div>
	<div class="form-group">
		<label for="precio" class="col-md-4 control-label">Precio</label>
		<div class="col-md-8">
			<input type="text" name="precio" value="<?php //echo $this->input->post('precio'); ?>" class="form-control" id="precio" />
		</div>
	</div>
	<div class="form-group">
		<label for="articulocol" class="col-md-4 control-label">Articulocol</label>
		<div class="col-md-8">
			<input type="text" name="articulocol" value="<?php //echo $this->input->post('articulocol'); ?>" class="form-control" id="articulocol" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	-->






	<h1 class="text-center">REGISTRAR ARTICULO</h1>
<form>
<div class="container">
	<div class="form-group">
		<label for="condicion" class="col-md-4 control-label">Condicion</label>
		<div class="col-md-8">
			<input type="checkbox" checked name="condicion" value="1" id="condicion" />
		</div>
	</div>
    <div class="form-group">
      <label>Nombre</label>
      <input type="text" class="form-control" name="nombre" id="nombre_articulo" placeholder="Nombre">
    </div>
    <div class="form-group">
      <label>Descripcion</label>
      <input type="text" class="form-control" name="descripcion" id="desc_articulo" placeholder="Descripcion">
    </div>
  <div class="row">
  <div class="form-group col">
    <label>Codigo</label>
    <input type="text" name="codigo" class="form-control" id="codigo_articulo" placeholder="Codigo">
  </div>
  
  <div class="form-group col">
    <label>Categoria</label>
    <select name="idcategoria" class="form-control" id="cat_articulo">
				<option value="">Seleccione categoria</option>
				<?php 
				foreach($all_categorias as $categoria)
				{
					$selected = ($categoria['idcategoria'] == $this->input->post('idcategoria')) ? ' selected="selected"' : "";

					echo '<option value="'.$categoria['idcategoria'].'" '.$selected.'>'.$categoria['nombre'].'</option>';
				} 
				?>
			</select>
  </div>
  <div class="form-group col">
    <label>Precio</label>
    <input type="text" name="precio" class="form-control" id="precio_articulo" placeholder="0">
  </div>
  <div class="form-group col">
    <label>stock</label>
    <input type="text" name="stock" class="form-control" id="stock" placeholder="0">
  </div>
  </div>
  
  <div class="text-center">
  <br>
  <button type="submit" class="btn btn-success" id="agregar_articulo">REGISTRAR</button>
  <span id="cancelar" class="btn btn-danger"><a href="<?=base_url()?>" title="" class="text-white">CANCELAR</a> </span>
  <br>
  </div> 
</div>
</form>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#agregar_articulo').click(function(event) {
			valor = true;
			if (!$('#nombre_articulo').val()) {
				alert("No se olvide del nombre del Articulo");
				$('#nombre_articulo').focus();
			valor = false;
			}
			else if (!$('#desc_articulo').val()) {
				alert("No se olvide de la Descripcion");
				$('#desc_articulo').focus();
			valor = false;
			}
			else if (!$('#codigo_articulo').val()) {
				alert("No se olvide del Codigo del Articulo");
				$('#codigo_articulo').focus();
			valor = false;
			}
			else if (!$('#cat_articulo').val()) {
				alert("No se olvide de la Categoria del Articulo");
				$('#cat_articulo').focus();
			valor = false;
			}
			else if (!$('#precio_articulo').val()) {
				alert("No se olvide del Precio del Articulo");
				$('#precio_articulo').focus();
			valor = false;
			}
			else if (!$('#stock').val()) {
				alert("No se olvide del Stock del Articulo");
				$('#stock').focus();
			valor = false;
			}
			return valor;
		});
		});
	</script>