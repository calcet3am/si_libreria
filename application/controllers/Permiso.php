<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Permiso extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Permiso_model');
    } 

    /*
     * Listing of permisos
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('permiso/index?');
        $config['total_rows'] = $this->Permiso_model->get_all_permisos_count();
        $this->pagination->initialize($config);

        $data['permisos'] = $this->Permiso_model->get_all_permisos($params);
        
        $data['_view'] = 'permiso/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new permiso
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nombre' => $this->input->post('nombre'),
            );
            
            $permiso_id = $this->Permiso_model->add_permiso($params);
            redirect('permiso/index');
        }
        else
        {            
            $data['_view'] = 'permiso/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a permiso
     */
    function edit($idpermiso)
    {   
        // check if the permiso exists before trying to edit it
        $data['permiso'] = $this->Permiso_model->get_permiso($idpermiso);
        
        if(isset($data['permiso']['idpermiso']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nombre' => $this->input->post('nombre'),
                );

                $this->Permiso_model->update_permiso($idpermiso,$params);            
                redirect('permiso/index');
            }
            else
            {
                $data['_view'] = 'permiso/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The permiso you are trying to edit does not exist.');
    } 

    /*
     * Deleting permiso
     */
    function remove($idpermiso)
    {
        $permiso = $this->Permiso_model->get_permiso($idpermiso);

        // check if the permiso exists before trying to delete it
        if(isset($permiso['idpermiso']))
        {
            $this->Permiso_model->delete_permiso($idpermiso);
            redirect('permiso/index');
        }
        else
            show_error('The permiso you are trying to delete does not exist.');
    }
    
}
