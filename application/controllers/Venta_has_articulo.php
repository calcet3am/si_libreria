<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Venta_has_articulo extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Venta_has_articulo_model');
    } 

    /*
     * Listing of venta_has_articulos
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('venta_has_articulo/index?');
        $config['total_rows'] = $this->Venta_has_articulo_model->get_all_venta_has_articulos_count();
        $this->pagination->initialize($config);

        $data['venta_has_articulos'] = $this->Venta_has_articulo_model->get_all_venta_has_articulos($params);
        
        $data['_view'] = 'venta_has_articulo/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new venta_has_articulo
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
            );
            
            $venta_has_articulo_id = $this->Venta_has_articulo_model->add_venta_has_articulo($params);
            redirect('venta_has_articulo/index');
        }
        else
        {            
            $data['_view'] = 'venta_has_articulo/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a venta_has_articulo
     */
    function edit($idventa)
    {   
        // check if the venta_has_articulo exists before trying to edit it
        $data['venta_has_articulo'] = $this->Venta_has_articulo_model->get_venta_has_articulo($idventa);
        
        if(isset($data['venta_has_articulo']['idventa']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                );

                $this->Venta_has_articulo_model->update_venta_has_articulo($idventa,$params);            
                redirect('venta_has_articulo/index');
            }
            else
            {
                $data['_view'] = 'venta_has_articulo/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The venta_has_articulo you are trying to edit does not exist.');
    } 

    /*
     * Deleting venta_has_articulo
     */
    function remove($idventa)
    {
        $venta_has_articulo = $this->Venta_has_articulo_model->get_venta_has_articulo($idventa);

        // check if the venta_has_articulo exists before trying to delete it
        if(isset($venta_has_articulo['idventa']))
        {
            $this->Venta_has_articulo_model->delete_venta_has_articulo($idventa);
            redirect('venta_has_articulo/index');
        }
        else
            show_error('The venta_has_articulo you are trying to delete does not exist.');
    }
    
}
